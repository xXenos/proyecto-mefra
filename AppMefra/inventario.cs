﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class inventario : Form
    {
        public int tipo { get; set; }
        MySqlDataAdapter msqlad;
        MySqlCommandBuilder msqlob;
        MySqlCommand cmsql;
        DataTable dt;
        int id = 0;
        string nombre;
        string preciounitario;
        string cantidad;
        

        public inventario()
        {
            InitializeComponent();
            rellenar();
        }

        public void rellenar()
        {
            cmsql = new MySqlCommand("SELECT * FROM tb_inventario", conexion.entrarconexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            invenbox.DataSource = dt;
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                msqlob = new MySqlCommandBuilder(msqlad);
                msqlad.Update(dt);
            }catch(MySqlException ex)
            {
                MessageBox.Show("Error, no se pudo actualizar la base de datos. Error: " + ex.Message);
            }
            rellenar();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            btn_aceptar.Visible = true;
            textBox1.Visible = true;
            label1.Visible = true;
        }

        private void btn_aceptar_Click(object sender, EventArgs e)
        {
            cmsql = new MySqlCommand("DELETE FROM tb_inventario where nombre = @nombre", conexion.entrarconexion());
            cmsql.Parameters.AddWithValue("@nombre", textBox1.Text);
            cmsql.ExecuteNonQuery();
            textBox1.Visible = false;
            textBox1.Text = "";
            btn_aceptar.Visible = false;
            label1.Visible = false;
        }

        private void btn_regresar_Click(object sender, EventArgs e)
        {
            menu home = new menu();
            home.Show();
            this.Dispose();
        }
    }
}
