﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class camiones : Form
    {
        public int tipo { get; set; }
        
        MySqlCommand cmsql;
        string camionven;
        string garra;
        string bols;
        string max;
        string fec;
        MySqlDataAdapter msqlad;
        MySqlCommandBuilder msqlcb;
        DataTable dt;

        public camiones()
        {
            InitializeComponent();
            label5.Text = DateTime.Now.ToString();
            rellenar();
        }

        public void rellenar()
        {
            cmsql = new MySqlCommand("SELECT camion, garrafones, bolsas, maxycold, fecha FROM tb_camiones", conexion.entrarconexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            datav.DataSource = dt;
        }

        private void dataGridView2_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            camionven = datav.Rows[e.RowIndex].Cells[0].Value.ToString();
            garra = datav.Rows[e.RowIndex].Cells[1].Value.ToString();
            bols = datav.Rows[e.RowIndex].Cells[2].Value.ToString();
            max = datav.Rows[e.RowIndex].Cells[3].Value.ToString();
            fec = datav.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(DataGridViewRow row in dataGridView1.Rows)
                {
                    string camionp = Convert.ToString(row.Cells[0].Value);
                    int cantigarrafon = Convert.ToInt32(row.Cells[1].Value);
                    int cantibolsa = Convert.ToInt32(row.Cells[2].Value);
                    int cantimax = Convert.ToInt32(row.Cells[3].Value);
                    cmsql = new MySqlCommand("INSERT INTO tb_camiones (camion,garrafones,bolsas,maxycold,fecha) VALUES (@camion,@garrafones,@bolsas,@maxycold,@fecha)", conexion.entrarconexion());
                    cmsql.Parameters.AddWithValue("@camion", camionp);
                    cmsql.Parameters.AddWithValue("@garrafones", cantigarrafon);
                    cmsql.Parameters.AddWithValue("@bolsas", cantibolsa);
                    cmsql.Parameters.AddWithValue("@maxycold", cantimax);
                    cmsql.Parameters.AddWithValue("@fecha", Convert.ToDateTime(label5.Text));
                    cmsql.ExecuteNonQuery();
                }
                MessageBox.Show("Datos Guardados");
                limpiar();

            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Error al registrar los datos, Err: " + ex.Message);
            }
        }

        public void limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox1.Text == "0" && textBox2.Text == "" && textBox3.Text == "" || textBox4.Text == "")
            {
                MessageBox.Show("Debe de llenar correctamente los campos!");
            }else
            {
                dataGridView1.Rows.Add(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
            }

        }

        private void camiones_Load(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                msqlcb = new MySqlCommandBuilder(msqlad);
                msqlad.Update(dt);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al actualizar datos!, Err: " + ex.Message);
            }

            rellenar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }

}
