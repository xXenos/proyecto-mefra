﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class ventauni : Form
    {

        public int tipo { get; set; }
        MySqlDataAdapter msqlad;
        MySqlCommand cmsql;
        MySqlDataReader msqlrd;
        DataTable dt;
        string id_producto, producto, precioCU, max;
        double res;

        private void button5_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿De verdad quiere confirmar la venta?", "Alerta!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                   
                    button3.Enabled = true;
                    button5.Enabled = false;
                    button6.Enabled = false;
                    button1.Enabled = false;
                    textBox2.Enabled = false;

                    try
                    {
                        cmsql = new MySqlCommand("INSERT INTO tb_ventas(total,fecha) VALUES (@total,@fecha)", conexion.entrarconexion());
                        cmsql.Parameters.AddWithValue("@total", textBox3.Text);
                        cmsql.Parameters.AddWithValue("@fecha", Convert.ToDateTime(label6.Text));
                        cmsql.ExecuteNonQuery();
                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show("Error al insertar datos!, Err: " + ex.Message);
                    }

                    msqlad = new MySqlDataAdapter("SELECT MAX(id_ventas) FROM tb_ventas", conexion.entrarconexion());
                    dt = new DataTable();
                    msqlad.Fill(dt);
                    max = dt.Rows[0][0].ToString();
                
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            consultaProducto(1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int id_inventario_unit = Convert.ToInt32(row.Cells[0].Value);
                    int cantidadunit = Convert.ToInt32(row.Cells[3].Value);
                    double precioventaunit = Convert.ToDouble(row.Cells[2].Value);
                    double totalunit = Convert.ToDouble(row.Cells[4].Value);
                    string p = Convert.ToString(row.Cells[5].Value);
                    cmsql = new MySqlCommand("INSERT INTO tb_venta_unitaria(id_ventas,id_inventario,cantidad,precioventa,total,fecha,vendedor) VALUES (@id_ventas,@id_inventario_menu,@cantidad,@precioventa,@total,@fecha,@vendedor)", conexion.entrarconexion());
                    cmsql.Parameters.AddWithValue("@id_ventas", max);
                    cmsql.Parameters.AddWithValue("@id_inventario_menu", id_inventario_unit);
                    cmsql.Parameters.AddWithValue("@cantidad", cantidadunit);
                    cmsql.Parameters.AddWithValue("@precioventa", precioventaunit);
                    cmsql.Parameters.AddWithValue("@total", totalunit);
                    cmsql.Parameters.AddWithValue("@fecha", Convert.ToDateTime(label6.Text));
                    cmsql.Parameters.AddWithValue("@vendedor", p);
                    cmsql.ExecuteNonQuery();

                }

                MessageBox.Show("Venta Guardada!");
                limpiar();
                button1.Enabled = true;
                textBox2.Enabled = true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al guardar datos!, Err: " + ex.Message);
            }

        }

        public void consultaProducto(int id)
        {
            cmsql = new MySqlCommand("SELECT nombre FROM tb_inventario WHERE id_inventario =" + id, conexion.entrarconexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            textBox1.Text = dt.Rows[0][0].ToString();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            limpiar();
            button1.Enabled = true;
            textBox2.Enabled = true;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            consultaProducto(4);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            consultaProducto(5);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "" || textBox2.Text == "0")
            {
                MessageBox.Show("No se puede agregar producto, a causa de que no especifico una cantidad mayor a 0!");
            }
            else
            {
                cmsql = new MySqlCommand("SELECT * FROM tb_inventario WHERE nombre = @nombre", conexion.entrarconexion());
                cmsql.Parameters.AddWithValue("@nombre", textBox1.Text);
                msqlrd = cmsql.ExecuteReader();
                if (msqlrd.Read())
                {
                    id_producto = msqlrd["id_inventario"].ToString();
                    producto = msqlrd["nombre"].ToString();
                    precioCU = msqlrd["precio_unitario"].ToString();

                }

                double result;
                result = float.Parse(precioCU) * int.Parse(textBox2.Text);
                result = Math.Round(result, 2);
                dataGridView1.Rows.Add(id_producto, producto, precioCU, textBox2.Text, result.ToString(), txt_vendedor.Text);
                double a = 0;
                foreach (DataGridViewRow row2 in dataGridView1.Rows)
                {
                    a += Convert.ToDouble(row2.Cells["subtotal"].Value);
                }
                textBox3.Text = a.ToString();
                textBox1.Text = "";
                textBox2.Text = "";
                txt_vendedor.Text = "";
                textBox4.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = true;
            }
        }

        

        public ventauni()
        {
            InitializeComponent();
            label6.Text = DateTime.Now.ToString();
        }

        public void limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            dataGridView1.Rows.Clear();
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox4.Enabled = false;
            button3.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
        }
    }
}
