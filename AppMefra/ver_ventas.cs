﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class ver_ventas : Form
    {
        MySqlDataAdapter msqlad;
        MySqlCommand cmsql;
        MySqlCommandBuilder msqlcb;
        DataTable dt;
        public ver_ventas()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;
            string day = date.Day.ToString();
            string month = date.Month.ToString();
            string year = date.Year.ToString();

            string Dateresult = year + month + day;

            cmsql = new MySqlCommand("SELECT total, fecha FROM tb_ventas WHERE fecha = " + Dateresult, conexion.entrarconexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            dataGridView1.DataSource = dt;

            double a = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                a += Convert.ToDouble(row.Cells["total"].Value);
            }
            textBox1.Text = a.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            menu m = new menu();
            m.Show();
            this.Dispose();
        }
    }
}
