﻿
namespace AppMefra
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_inventario = new System.Windows.Forms.Button();
            this.btn_ver_ventas = new System.Windows.Forms.Button();
            this.btn_recordatorio = new System.Windows.Forms.Button();
            this.btn_salir = new System.Windows.Forms.Button();
            this.btn_agregar_venta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.hora_fecha = new System.Windows.Forms.Timer(this.components);
            this.h_f = new System.Windows.Forms.Label();
            this.h_f2 = new System.Windows.Forms.Label();
            this.dia_semana = new System.Windows.Forms.Label();
            this.Camiones = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_inventario
            // 
            this.btn_inventario.Location = new System.Drawing.Point(62, 61);
            this.btn_inventario.Name = "btn_inventario";
            this.btn_inventario.Size = new System.Drawing.Size(75, 23);
            this.btn_inventario.TabIndex = 0;
            this.btn_inventario.Text = "Inventario";
            this.btn_inventario.UseVisualStyleBackColor = true;
            this.btn_inventario.Click += new System.EventHandler(this.btn_inventario_Click);
            // 
            // btn_ver_ventas
            // 
            this.btn_ver_ventas.Location = new System.Drawing.Point(62, 192);
            this.btn_ver_ventas.Name = "btn_ver_ventas";
            this.btn_ver_ventas.Size = new System.Drawing.Size(75, 23);
            this.btn_ver_ventas.TabIndex = 2;
            this.btn_ver_ventas.Text = "Ver ventas";
            this.btn_ver_ventas.UseVisualStyleBackColor = true;
            this.btn_ver_ventas.Click += new System.EventHandler(this.btn_ver_ventas_Click);
            // 
            // btn_recordatorio
            // 
            this.btn_recordatorio.Location = new System.Drawing.Point(62, 256);
            this.btn_recordatorio.Name = "btn_recordatorio";
            this.btn_recordatorio.Size = new System.Drawing.Size(83, 23);
            this.btn_recordatorio.TabIndex = 3;
            this.btn_recordatorio.Text = "Recordatorios";
            this.btn_recordatorio.UseVisualStyleBackColor = true;
            this.btn_recordatorio.Click += new System.EventHandler(this.btn_recordatorio_Click);
            // 
            // btn_salir
            // 
            this.btn_salir.Location = new System.Drawing.Point(434, 358);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(75, 23);
            this.btn_salir.TabIndex = 4;
            this.btn_salir.Text = "salir";
            this.btn_salir.UseVisualStyleBackColor = true;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // btn_agregar_venta
            // 
            this.btn_agregar_venta.Location = new System.Drawing.Point(62, 125);
            this.btn_agregar_venta.Name = "btn_agregar_venta";
            this.btn_agregar_venta.Size = new System.Drawing.Size(75, 23);
            this.btn_agregar_venta.TabIndex = 1;
            this.btn_agregar_venta.Text = "Vender";
            this.btn_agregar_venta.UseVisualStyleBackColor = true;
            this.btn_agregar_venta.Click += new System.EventHandler(this.btn_agregar_venta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Rosewood Std Regular", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(169, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 37);
            this.label1.TabIndex = 5;
            this.label1.Text = "AppMEFRA";
            // 
            // hora_fecha
            // 
            this.hora_fecha.Enabled = true;
            this.hora_fecha.Tick += new System.EventHandler(this.hora_fecha_Tick);
            // 
            // h_f
            // 
            this.h_f.AutoSize = true;
            this.h_f.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h_f.Location = new System.Drawing.Point(226, 360);
            this.h_f.Name = "h_f";
            this.h_f.Size = new System.Drawing.Size(53, 24);
            this.h_f.TabIndex = 6;
            this.h_f.Text = "Reloj";
            // 
            // h_f2
            // 
            this.h_f2.AutoSize = true;
            this.h_f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h_f2.Location = new System.Drawing.Point(12, 358);
            this.h_f2.Name = "h_f2";
            this.h_f2.Size = new System.Drawing.Size(64, 24);
            this.h_f2.TabIndex = 7;
            this.h_f2.Text = "Fecha";
            // 
            // dia_semana
            // 
            this.dia_semana.AutoSize = true;
            this.dia_semana.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dia_semana.Location = new System.Drawing.Point(92, 360);
            this.dia_semana.Name = "dia_semana";
            this.dia_semana.Size = new System.Drawing.Size(114, 24);
            this.dia_semana.TabIndex = 8;
            this.dia_semana.Text = "Dia_semana";
            // 
            // Camiones
            // 
            this.Camiones.Location = new System.Drawing.Point(62, 311);
            this.Camiones.Name = "Camiones";
            this.Camiones.Size = new System.Drawing.Size(83, 23);
            this.Camiones.TabIndex = 9;
            this.Camiones.Text = "Camiones";
            this.Camiones.UseVisualStyleBackColor = true;
            this.Camiones.Click += new System.EventHandler(this.Camiones_Click);
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 393);
            this.Controls.Add(this.Camiones);
            this.Controls.Add(this.dia_semana);
            this.Controls.Add(this.h_f2);
            this.Controls.Add(this.h_f);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_agregar_venta);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.btn_recordatorio);
            this.Controls.Add(this.btn_ver_ventas);
            this.Controls.Add(this.btn_inventario);
            this.Name = "menu";
            this.Text = "menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_inventario;
        private System.Windows.Forms.Button btn_ver_ventas;
        private System.Windows.Forms.Button btn_recordatorio;
        private System.Windows.Forms.Button btn_salir;
        private System.Windows.Forms.Button btn_agregar_venta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer hora_fecha;
        private System.Windows.Forms.Label h_f;
        private System.Windows.Forms.Label h_f2;
        private System.Windows.Forms.Label dia_semana;
        private System.Windows.Forms.Button Camiones;
    }
}