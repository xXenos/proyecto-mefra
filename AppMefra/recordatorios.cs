﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class recordatorios : Form
    {
        public int tipo { get; set; }
        MySqlDataAdapter msqlad;
        MySqlCommand cmsql;
        MySqlCommandBuilder msqlob;
        DataTable dt;
        int id = 0;
        string descripcion;
        string fecha;
        public recordatorios()
        {
            InitializeComponent();
            rellenar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            menu m = new menu();
            m.Show();
            this.Dispose();
        }

        public void rellenar()
        {
            cmsql = new MySqlCommand("SELECT * FROM tb_recordatorio", conexion.entrarconexion());
            msqlad = new MySqlDataAdapter(cmsql);
            dt = new DataTable();
            msqlad.Fill(dt);
            recorbox.DataSource = dt;
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            try
            {
                msqlob = new MySqlCommandBuilder(msqlad);
                msqlad.Update(dt);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error, no se pudo actualizar la base de datos. Error: " + ex.Message);
            }
            rellenar();
        }

        private void btn_aceptar_Click(object sender, EventArgs e)
        {
            cmsql = new MySqlCommand("DELETE FROM tb_recordatorio where descripcion = @descripcion", conexion.entrarconexion());
            cmsql.Parameters.AddWithValue("@descripcion", textBox1.Text);
            cmsql.ExecuteNonQuery();
            textBox1.Visible = false;
            textBox1.Text = "";
            btn_aceptar.Visible = false;
            label1.Visible = false;
        }

        private void recorbox_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            descripcion = recorbox.Rows[e.RowIndex].Cells[0].Value.ToString();
            fecha = recorbox.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void btn_eliminar_Click(object sender, EventArgs e)
        {
            btn_aceptar.Visible = true;
            textBox1.Visible = true;
            label1.Visible = true;
        }
    }
}
