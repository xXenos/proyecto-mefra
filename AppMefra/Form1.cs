﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AppMefra
{
    public partial class inicio : Form
    {
        public inicio()
        {
            InitializeComponent();
            conexion.entrarconexion();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conexion.CerrrarConexion();
            Application.Exit();
        }

        private void btn_entrar_Click(object sender, EventArgs e)
        {
            if (txt_user.Text == "" && txt_contra.Text == "")
            {
                MessageBox.Show("Ingrese todos los parametros");
            }
            else if (txt_user.Text == "")
            {
                MessageBox.Show("Ingrese el usuario");
            }
            else if (txt_contra.Text == "")
            {
                MessageBox.Show("Ingrese la contraseña");
            }
            else
            {
                MySqlCommand cmsql = new MySqlCommand("SELECT usuario, permiso FROM tb_usuario WHERE usuario = '" + txt_user.Text + "' AND password = '" + txt_contra.Text + "'", conexion.entrarconexion());
                MySqlDataAdapter msqlad = new MySqlDataAdapter(cmsql);
                DataTable dt = new DataTable();
                msqlad.Fill(dt);
                if (dt.Rows.Count == 1)
                {
                    if(dt.Rows[0][1].ToString() == "gerente")
                    {
                        menu f1 = new menu();
                        f1.tipo = 1;
                        f1.Show();
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("informacion incorrecta");
                }
            }
        }
    }
}
