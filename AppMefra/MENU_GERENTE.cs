﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppMefra
{
    public partial class menu : Form
    {
        public int tipo { get; set; }
        public menu()
        {
            InitializeComponent();
        }

        private void hora_fecha_Tick(object sender, EventArgs e)
        {
            h_f.Text = DateTime.Now.ToLongTimeString();
            dia_semana.Text = DateTime.Now.ToShortDateString();
            h_f2.Text = DateTime.Now.ToString("dddd");

            
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            conexion.CerrrarConexion();
            inicio login = new inicio();
            login.Show();
            this.Dispose();
        }

        private void btn_inventario_Click(object sender, EventArgs e)
        {
            inventario invent = new inventario();
            invent.tipo = 1;
            invent.Show();
            this.Dispose();
        }

        private void btn_agregar_venta_Click(object sender, EventArgs e)
        {
            ventauni ventauni = new ventauni();
             ventauni.Show();
            
        }

        private void btn_ver_ventas_Click(object sender, EventArgs e)
        {
            ver_ventas ventas = new ver_ventas();
            ventas.Show();
            this.Dispose();
        }

        private void btn_recordatorio_Click(object sender, EventArgs e)
        {
            recordatorios calendario = new recordatorios();
            calendario.tipo = 1;
            calendario.Show();
            this.Dispose();
        }

        private void Camiones_Click(object sender, EventArgs e)
        {
            camiones cam = new camiones();
            cam.Show();
        }
    }
}
